from util import *
from werewolf_solver.model.game_information import GameInformation, Casting, COTable, Executed, Corpses, Assumptions
from werewolf_solver.solver.solver import search_cases


def test_demo1():
	casting: Casting = {
		"Knight": 1,
		"FortuneTeller": 1,
		"Medium": 1,
		"Sharer": 2,
		"Madman": 1,
		"Werewolf": 2
	}
	co_table: COTable = {
		"FortuneTeller": [{
			"index": 1,
			"results": [{"target": 4, "is_werewolf": False}, {"target": 5, "is_werewolf": False}]
		}, {
			"index": 2,
			"results": [{"target": 7, "is_werewolf": True}, {"target": 5, "is_werewolf": False}]
		}, {
			"index": 3,
			"results": [{"target": 7, "is_werewolf": True}, {"target": 5, "is_werewolf": False}]
		}],
		"Medium": [{
			"index": 4,
			"results": [{"target": 2, "is_werewolf": False}]
		}],
		"Sharer": [{
			"index": 5,
			"results": [{"partner": 6}]
		}, {
			"index": 6,
			"results": [{"partner": 5}]
		}],
		"Knight": [{
			"index": 7,
			"results": [{"target": 1}]
		}]
	}
	executed: Executed = [[2]]
	corpses: Corpses = [[0], []]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["FortuneTeller", "Madman", "Werewolf"], "truth": True
	}, {
		"target": 2, "roles": ["FortuneTeller", "Madman", "Werewolf"], "truth": True
	}, {
		"target": 3, "roles": ["FortuneTeller", "Madman", "Werewolf"], "truth": True
	}, {
		"target": 4, "roles": ["Medium", "Madman", "Werewolf"], "truth": True
	}, {
		"target": 5, "roles": ["Sharer", "Madman", "Werewolf"], "truth": True
	}, {
		"target": 6, "roles": ["Sharer", "Madman", "Werewolf"], "truth": True
	}, {
		"target": 7, "roles": ["Knight", "Madman", "Werewolf"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	assert filter_result(search_cases(info), "players_role") == [
		['Medium', 'FortuneTeller', 'Werewolf', 'Werewolf', 'Madman', 'Sharer', 'Sharer', 'Knight']
	]


# def test_demo_2():
# 	casting: Casting = {
# 		"Knight": 1,
# 		"FortuneTeller": 1,
# 		"Medium": 1,
# 		"Madman": 1,
# 		"Werewolf": 2,
# 		"Fox": 1,
# 		"Villager": 5
# 	}
# 	co_table: COTable = {
# 		"FortuneTeller": [{
# 			"index": 6,
# 			"results": [
# 				{"target": 1, "is_werewolf": False},
# 				{"target": 10, "is_werewolf": False},
# 				{"target": 8, "is_werewolf": True},
# 				{"target": 11, "is_werewolf": True},
# 			]
# 		}],
# 		"Medium": [{
# 			"index": 3,
# 			"results": [
# 				{"target": 7, "is_werewolf": False},
# 				{"target": 4, "is_werewolf": False}
# 			]
# 		}],
# 		"Knight": []
# 	}
# 	executed: Executed = [
# 		[7],
# 		[4],
# 		[8]
# 	]
# 	corpses: Corpses = [
# 		[0],
# 		[],
# 		[10],
# 		[3]
# 	]
# 	assumptions: Assumptions = [
# 		# {"target": 0, "roles": ["FortuneTeller"], "truth": False},
# 		{"target": 1, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 2, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 3, "roles": ["Medium", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 4, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 5, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 6, "roles": ["FortuneTeller", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 7, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 8, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 9, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 10, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 		{"target": 11, "roles": ["Villager", "Knight", "Madman", "Werewolf", "Fox"], "truth": True},
# 	]
# 	info: GameInformation = {
# 		"casting": casting,
# 		"co_table": co_table,
# 		"executed": executed,
# 		"corpses": corpses,
# 		"assumptions": assumptions
# 	}
# 	print(search_cases(info))
