def list_to_set(results):
	return set([str(case) for case in results])


def filter_result(results, key):
	return list(map(lambda x: x[key], results))


def is_decided_role(results, index, role):
	return all([result["players_role"][index] == role for result in results]) and len(results) != 0


def is_decided_attacked_player(results, day, player_index):
	"""
	dayは1-indexed
	"""
	return all([result["attacked_players"][day - 1] == player_index for result in results]) and len(results) != 0
