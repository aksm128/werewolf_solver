from util import *
from werewolf_solver.model.game_information import GameInformation, Casting, COTable, Executed, Corpses, Assumptions
from werewolf_solver.solver.solver import search_cases


def test_casting():
	casting: Casting = {
		"Werewolf": 1,
		"Villager": 3,
	}
	co_table: COTable = {}
	executed: Executed = []
	corpses: Corpses = [[0]]
	assumptions: Assumptions = []
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	expected_result = [
		["Villager", "Werewolf", "Villager", "Villager"],
		["Villager", "Villager", "Werewolf", "Villager"],
		["Villager", "Villager", "Villager", "Werewolf"],
	]
	assert list_to_set(filter_result(search_cases(info), "players_role")) == list_to_set(expected_result)


def test_assumption():
	casting: Casting = {
		"Werewolf": 1,
		"Villager": 3,
	}
	co_table: COTable = {}
	executed: Executed = []
	corpses: Corpses = [[0]]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["Werewolf"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	expected_result = [
		["Villager", "Werewolf", "Villager", "Villager"],
	]
	assert list_to_set(filter_result(search_cases(info), "players_role")) == list_to_set(expected_result)


def test_fortune_teller():
	casting: Casting = {
		"Werewolf": 1,
		"Villager": 2,
		"FortuneTeller": 1,
	}
	co_table: COTable = {
		"FortuneTeller": [{
			"index": 1,
			"results": [{"target": 2, "is_werewolf": True}]
		}]
	}
	executed: Executed = []
	corpses: Corpses = [[0]]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["FortuneTeller"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	assert is_decided_role(search_cases(info), 2, "Werewolf")


def test_medium():
	casting: Casting = {
		"Werewolf": 2,
		"Villager": 5,
		"Medium": 1,
	}
	co_table: COTable = {
		"Medium": [{
			"index": 1,
			"results": [{"target": 2, "is_werewolf": True}]
		}]
	}
	executed: Executed = []
	corpses: Corpses = [[0]]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["Medium"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	assert is_decided_role(search_cases(info), 2, "Werewolf")


def test_sharer():
	casting: Casting = {
		"Werewolf": 1,
		"Villager": 1,
		"Sharer": 2,
	}
	co_table: COTable = {
		"Sharer": [{
			"index": 1,
			"results": [{"partner": 2}]
		}, {
			"index": 2,
			"results": [{"partner": 1}]
		}]
	}
	executed: Executed = []
	corpses: Corpses = [[0]]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["Sharer", "Werewolf"], "truth": True
	}, {
		"target": 2, "roles": ["Sharer", "Werewolf"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	expected_result = [
		["Villager", "Sharer", "Sharer", "Werewolf"]
	]
	assert list_to_set(filter_result(search_cases(info), "players_role")) == list_to_set(expected_result)


def test_knight_save():
	casting: Casting = {
		"Werewolf": 1,
		"Villager": 3,
		"Knight": 1,
	}
	co_table: COTable = {
		"Knight": [{
			"index": 1,
			"results": [{"target": 2}]
		}]
	}
	executed: Executed = [[4]]
	corpses: Corpses = [[0], []]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["Knight"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	expected_result = [
		["Villager", "Knight", "Villager", "Werewolf", "Villager"],
		["Villager", "Knight", "Villager", "Villager", "Werewolf"]
	]
	results = search_cases(info)
	assert list_to_set(filter_result(results, "players_role")) == list_to_set(expected_result)
	# 2日目夜に噛まれたのは2
	assert is_decided_attacked_player(results, 2, 2)


def test_knight_alive():
	casting: Casting = {
		"Werewolf": 1,
		"Villager": 4,
		"Knight": 1,
	}
	co_table: COTable = {
		"Knight": [{
			"index": 1,
			"results": [{"target": 2}]
		}]
	}
	executed: Executed = [[4]]
	corpses: Corpses = [[0], []]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["Knight", "Werewolf"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	assert is_decided_role(search_cases(info), 0, "Villager")


def test_knight_collapse():
	casting: Casting = {
		"Werewolf": 1,
		"Villager": 4,
		"Knight": 1,
	}
	co_table: COTable = {
		"Knight": [{
			"index": 1,
			"results": [{"target": 2}]
		}]
	}
	executed: Executed = [[4]]
	corpses: Corpses = [[0], [2]]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["Knight", "Werewolf"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	results = search_cases(info)
	assert is_decided_role(results, 1, "Werewolf")
	# 2日目夜に噛まれたのは2
	assert is_decided_attacked_player(results, 2, 2)


def test_fox_attacked():
	casting: Casting = {
		"Werewolf": 1,
		"Villager": 3,
		"Fox": 1,
	}
	co_table: COTable = {}
	executed: Executed = [[4]]
	corpses: Corpses = [[0], []]
	assumptions: Assumptions = [{
		"target": 1, "roles": ["Fox"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	# 2日目に噛まれたのは1(妖狐)
	assert is_decided_attacked_player(search_cases(info), 2, 1)


def test_fox_fortune_told():
	casting: Casting = {
		"Werewolf": 1,
		"FortuneTeller": 1,
		"Villager": 4,
		"Fox": 1,
	}
	co_table: COTable = {
		"FortuneTeller": [{
			"index": 4,
			"results": [{"target": 2, "is_werewolf": False}]
		}]
	}
	executed: Executed = [[1]]
	corpses: Corpses = [[0], [2, 3]]
	assumptions: Assumptions = [{
		"target": 2, "roles": ["Fox"], "truth": True,
	}, {
		"target": 4, "roles": ["FortuneTeller"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	assert is_decided_attacked_player(search_cases(info), 2, 3)


# 1死体=呪殺GJか呪殺隠しか襲撃
def test_fox_fortune_told_gj_or_fortune_told_and_attack():
	casting: Casting = {
		"Werewolf": 1,
		"FortuneTeller": 1,
		"Knight": 1,
		"Villager": 3,
		"Fox": 1,
	}
	co_table: COTable = {
		"Knight": [{
			"index": 5,
			"results": [{"target": 4}]
		}],
		"FortuneTeller": [{
			"index": 4,
			"results": [{"target": 1, "is_werewolf": False}, {"target": 2, "is_werewolf": False}]
		}]
	}
	executed: Executed = [[1]]
	corpses: Corpses = [[0], [2]]
	assumptions: Assumptions = [{
		"target": 4, "roles": ["FortuneTeller"], "truth": True
	}, {
		"target": 5, "roles": ["Knight"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	# 2か4を噛んだ
	assert all([
		case["attacked_players"][1] == 2 or case["attacked_players"][1] == 4
		for case in search_cases(info)
	])


# 呪殺隠しでGJ
def test_fox_fortune_told_and_attack():
	casting: Casting = {
		"Werewolf": 1,
		"FortuneTeller": 1,
		"Knight": 1,
		"Villager": 3,
		"Fox": 1,
	}
	co_table: COTable = {
		"Knight": [{
			"index": 5,
			"results": [{"target": 2}]
		}],
		"FortuneTeller": [{
			"index": 4,
			"results": [{"target": 1, "is_werewolf": False}, {"target": 2, "is_werewolf": False}]
		}]
	}
	executed: Executed = [[1]]
	corpses: Corpses = [[0], [2]]
	assumptions: Assumptions = [{
		"target": 2, "roles": ["Fox"], "truth": True,
	}, {
		"target": 4, "roles": ["FortuneTeller"], "truth": True
	}, {
		"target": 5, "roles": ["Knight"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	assert is_decided_attacked_player(search_cases(info), 2, 2)


# 死体無し=GJか狐噛み
def test_gj_or_attacked_fox():
	casting: Casting = {
		"Werewolf": 1,
		"FortuneTeller": 1,
		"Knight": 1,
		"Villager": 3,
		"Fox": 1,
	}
	co_table: COTable = {
		"Knight": [{
			"index": 5,
			"results": [{"target": 4}]
		}],
		"FortuneTeller": [{
			"index": 4,
			"results": [{"target": 1, "is_werewolf": False}, {"target": 2, "is_werewolf": False}]
		}]
	}
	executed: Executed = [[1]]
	corpses: Corpses = [[0], []]
	assumptions: Assumptions = [{
		"target": 2, "roles": ["Fox"], "truth": True,
	}, {
		"target": 4, "roles": ["FortuneTeller"], "truth": True
	}, {
		"target": 5, "roles": ["Knight"], "truth": True
	}]
	info: GameInformation = {
		"casting": casting,
		"co_table": co_table,
		"executed": executed,
		"corpses": corpses,
		"assumptions": assumptions
	}
	# 2か4を噛んだ
	assert all([
		case["attacked_players"][1] == 2 or case["attacked_players"][1] == 4
		for case in search_cases(info)
	])
