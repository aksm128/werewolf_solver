from werewolf_solver.solver.solver import search_cases
# 配役 (占狼村村)
casting = {
	"Werewolf": 1,
	"Villager": 2,
	"FortuneTeller": 1,
}
# CO表 (プレイヤー1が占いCOをし、プレイヤー2黒と主張した)
co_table = {
	"FortuneTeller": [{
		"index": 1,
		"results": [{"target": 2, "is_werewolf": True}]
	}]
}
# 処刑したプレイヤー
executed = []
# 朝に死体になったプレイヤー (プレイヤー0は初日に襲撃された)
corpses = [[0]]
# 仮定 (プレイヤー1は占い師)
assumptions = [{
	"target": 1, "roles": ["FortuneTeller"], "truth": True
}]
# まとめた情報
info = {
	"casting": casting,
	"co_table": co_table,
	"executed": executed,
	"corpses": corpses,
	"assumptions": assumptions
}

# このような条件を満たす内訳を全探索
print(search_cases(info))
"""
結果 => [{
    # 占ったプレイヤー
    'fortune_told_players': [2], 
    # 襲撃したプレイヤー
    'attacked_players': [0], 
    # 配役
    'players_role': ['Villager', 'FortuneTeller', 'Werewolf', 'Villager']
}]
"""