from typing import *

from z3 import *

from werewolf_solver.model.assertion import Assertion
from werewolf_solver.model.game_information import GameInformation, count_player
from werewolf_solver.solver.builtin_role import builtin_roles
from werewolf_solver.solver.role import Role
from werewolf_solver.solver.status_symbols import StatusSymbols


def create_assertion_theory(players: List[DatatypeRef], status_symbols: Dict[str, StatusSymbols],
							assertion: Assertion) -> BoolRef:
	players_role: StatusSymbols = status_symbols["players_role"]
	if assertion["truth"]:
		return Or([
			players[assertion["target"]] == players_role.get(job_str)
			for job_str in assertion["roles"]
		])
	return And([
		players[assertion["target"]] != players_role.get(job_str)
		for job_str in assertion["roles"]
	])


def array_ref_to_items(array):
	if str(array.decl()) != "Store":
		return []
	return array_ref_to_items(array.arg(0)) + [(array.arg(1), array.arg(2))]


def search_cases(info: GameInformation, roles: List[Role] = None) -> List[Dict[str, Any]]:
	# if not is_valid_game_info(info):
	# 	raise Exception("invalid GameInformation.")

	if not roles:
		roles = builtin_roles

	casting = info["casting"]
	assumptions = info["assumptions"]

	solver = Solver()

	status_symbols = {
		"players_role": StatusSymbols("roles", list(casting.keys())),
		"players_index": IntSort()
	}

	possible_statuses = {
		# プレイヤーの役職
		"players_role": Array("players_role", IntSort(), status_symbols["players_role"].enum)
	}

	death_conditions = [[] for _ in range(len(info["corpses"]))]

	# 仮定を定義
	for assumption in assumptions:
		solver.add(
			create_assertion_theory(possible_statuses["players_role"], status_symbols, assumption)
		)

	# 配役の定義
	for role in status_symbols["players_role"].symbols:
		number = casting.get(str(role), 0)
		role_count = Sum([If(possible_statuses["players_role"][i] == role, 1, 0) for i in range(count_player(casting))])
		solver.add(role_count == number)

	# Symbolを定義する
	for role in roles:
		if role.name not in info["casting"]:
			continue
		result = role.create_symbol(info)
		if result is not None:
			name, symbols = result
			status_symbols[name] = symbols

	# 状態を定義する
	for role in roles:
		if role.name not in info["casting"]:
			continue
		result = role.create_status(info, status_symbols)
		if result is not None:
			name, status = result
			possible_statuses[name] = status

	# 制約を付加
	for role in roles:
		if role.name not in info["casting"]:
			continue
		assertion = role.create_assertions(info, possible_statuses, status_symbols)
		if assertion is not None:
			solver.add(assertion)

	# 死体ができる条件を付加
	for role in roles:
		if role.name not in info["casting"]:
			continue
		conditions = role.create_death_conditions(info, possible_statuses, status_symbols)
		if conditions is not None:
			for day in range(len(death_conditions)):
				death_conditions[day] += conditions[day]

	# 死体の整合性をチェック
	for day in range(len(death_conditions)):
		possible_corpses = Sum([If(condition, 1, 0) for condition in death_conditions[day]])
		solver.add(possible_corpses == len(info["corpses"][day]))

	# print(solver.assertions())
	# ケースを列挙
	answers: List[Dict[str, Any]] = []
	while solver.check() == sat:
		model = solver.model()
		answers.append({})
		# 役職ごとの状態(護衛先など)も結果に入れる
		for role in roles:
			if role.name not in info["casting"]:
				continue
			result = role.serialize_status(model, possible_statuses)
			if result is not None:
				name, obj = result
				answers[-1][name] = obj
		answers[-1]["players_role"] = [str(simplify(model[possible_statuses["players_role"]][i])) for i in
									   range(count_player(info["casting"]))]
		assertions = []
		for func_decl_ref in model.decls():
			obj = func_decl_ref()
			if isinstance(obj, ArrayRef):
				for idx, val in array_ref_to_items(model[obj]):
					assertions.append(obj[idx] == model[obj][idx])
				continue
			# print(obj, model[obj])
			# assertions.append(obj == model[obj])
		# print(answers[-1])
		solver.add(Not(And(assertions)))
	return answers
