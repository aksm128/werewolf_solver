from typing import *

from z3 import *

from werewolf_solver.model.assertion import Assertion
from werewolf_solver.model.game_information import GameInformation, get_alive_players
from werewolf_solver.solver.role import Role
from werewolf_solver.solver.status_symbols import StatusSymbols


class FortuneTellerResult(TypedDict):
	"""
	結果は初日から。
	"""
	target: int
	is_werewolf: bool


class FortuneTeller(Role):
	@property
	def name(self):
		return "FortuneTeller"

	@staticmethod
	def create_symbol(info: GameInformation) -> Optional[Tuple[str, StatusSymbols]]:
		return

	@staticmethod
	def create_status(info: GameInformation, status_symbols: Dict[str, StatusSymbols]) -> Optional[
		Tuple[str, Any]]:
		# X日目に占われたプレイヤーは誰か？という状態を持つ
		fortune_told_statuses = [
			Const(f"fortune_told_player_{i}", status_symbols["players_index"])
			for i in range(len(info["corpses"]))
		]
		return "fortune_told_players", fortune_told_statuses

	@staticmethod
	def serialize_status(model: ModelRef, possible_statuses: Dict[str, Any]) -> Optional[Tuple[str, Any]]:
		return "fortune_told_players", [model[told_player] for told_player in possible_statuses["fortune_told_players"]]

	def create_assertions(self, info: GameInformation, possible_statuses: Dict[str, Any],
						  status_symbols: Dict[str, StatusSymbols]) -> Optional[Assertion]:
		players_role = possible_statuses["players_role"]
		role_symbol = status_symbols["players_role"]
		assertions: List[Assertion] = []
		coming_outs = info["co_table"][self.name]
		for coming_out in coming_outs:
			player_assertions = []
			for day, result in enumerate(coming_out["results"]):
				# CO結果を付加
				if result["is_werewolf"]:
					player_assertions.append(
						players_role[result["target"]] == role_symbol.get("Werewolf"))
				else:
					player_assertions.append(
						players_role[result["target"]] != role_symbol.get("Werewolf"))
				# 占い先をCOによって定義
				player_assertions.append(
					possible_statuses["fortune_told_players"][day] == result["target"]
				)
			assertions.append(Implies(
				players_role[coming_out["index"]] == role_symbol.get("FortuneTeller"),
				And(player_assertions)
			))
		for day, told_player in enumerate(possible_statuses["fortune_told_players"]):
			# 生きてる人を占う
			assertions.append(Or([told_player == player for player in get_alive_players(info, day)]))

		return And(assertions)

	@staticmethod
	def create_death_conditions(info: GameInformation, possible_statuses: Dict[str, Any],
								status_symbols: Dict[str, StatusSymbols]) -> Optional[List[List[DatatypeRef]]]:
		players_role = possible_statuses["players_role"]
		role_symbol = status_symbols["players_role"]
		death_conditions = []
		for day, told_player in enumerate(possible_statuses["fortune_told_players"]):
			told_player: Int
			is_succeed_curse = False
			if "Fox" in info["casting"]:
				is_succeed_curse = Or(is_succeed_curse, players_role[told_player] == role_symbol.get("Fox"))
			death_conditions.append([is_succeed_curse])
		return death_conditions
