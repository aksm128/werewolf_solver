from .fortune_teller import FortuneTeller
from .fox import Fox
from .knight import Knight
from .madman import Madman
from .medium import Medium
from .sharer import Sharer
from .villager import Villager
from .werewolf import Werewolf

builtin_roles = [Villager(), FortuneTeller(), Medium(), Sharer(), Werewolf(), Madman(), Knight(), Fox()]
