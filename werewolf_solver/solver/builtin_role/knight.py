from typing import *

from z3 import *

from werewolf_solver.model.assertion import Assertion
from werewolf_solver.model.game_information import GameInformation, get_alive_players
from werewolf_solver.solver.role import Role
from werewolf_solver.solver.status_symbols import StatusSymbols


class KnightResult(TypedDict):
	"""
	結果は2日目から。
	"""
	target: int


class Knight(Role):
	"""

	"""

	@property
	def name(self):
		return "Knight"

	@staticmethod
	def create_symbol(info: GameInformation) -> Optional[Tuple[str, StatusSymbols]]:
		return

	@staticmethod
	def create_status(info: GameInformation, status_symbols: Dict[str, StatusSymbols]) -> Optional[
		Tuple[str, Any]]:
		# i番目のプレイヤーが狩人であるときのj日目に護衛したプレイヤーは誰か？という状態を持つ
		# TODO: 複数人対応(i番目のプレイヤーが狩人であるときのj日目の護衛先を持たせる)
		saved_statuses = [
			Const(f"saved_{i}", status_symbols["players_index"])
			for i in range(len(info["corpses"]) - 1)
		]
		return "saved_players", saved_statuses

	@staticmethod
	def serialize_status(model: ModelRef, possible_statuses: Dict[str, Any]) -> Optional[Tuple[str, Any]]:
		return "saved_players", [model[saved_player] for saved_player in possible_statuses["saved_players"]]

	def create_assertions(self, info: GameInformation, possible_statuses: Dict[str, Any],
						  status_symbols: Dict[str, StatusSymbols]) -> Optional[Assertion]:
		players_role = possible_statuses["players_role"]
		role_symbol = status_symbols["players_role"]
		assertions: List[Assertion] = []
		coming_outs = info["co_table"][self.name]
		for coming_out in coming_outs:
			# 護衛先をCOによって定義
			assertions.append(Implies(
				players_role[coming_out["index"]] == role_symbol.get("Knight"),
				And([
					saved_status == saved["target"]
					for saved, saved_status in
					zip(coming_out["results"], possible_statuses["saved_players"])
				]))
			)
		for day in range(1, len(info["corpses"])):
			alive_players = get_alive_players(info, day)
			# 襲撃先と護衛先が一緒＝護衛成功したらその時点で狩人は生存している
			assertions.append(Implies(
				possible_statuses["attacked_players"][day] == possible_statuses["saved_players"][day - 1],
				Or([
					players_role[player] == role_symbol.get("Knight")
					for player in alive_players
				])
			))
			# 生きている人を護衛する
			assertions.append(Or([possible_statuses["saved_players"][day - 1] == player for player in alive_players]))
		return And(assertions)

	@staticmethod
	def create_death_conditions(info: GameInformation, possible_statuses: Dict[str, Any],
								status_symbols: Dict[str, StatusSymbols]) -> Optional[List[List[DatatypeRef]]]:
		return
