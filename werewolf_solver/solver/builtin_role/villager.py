from typing import *

from z3 import *

from werewolf_solver.model.assertion import Assertion
from werewolf_solver.model.game_information import GameInformation
from werewolf_solver.solver.role import Role
from werewolf_solver.solver.status_symbols import StatusSymbols


class Villager(Role):
	@property
	def name(self) -> str:
		return "Villager"

	@staticmethod
	def create_symbol(info: GameInformation) -> Optional[Tuple[str, StatusSymbols]]:
		return

	@staticmethod
	def create_status(info: GameInformation, status_symbols: Dict[str, StatusSymbols]) -> Optional[
		Tuple[str, Any]]:
		return

	@staticmethod
	def serialize_status(model: ModelRef, possible_statuses: Dict[str, Any]) -> Optional[Tuple[str, Any]]:
		return

	@staticmethod
	def create_assertions(info: GameInformation, possible_statuses: Dict[str, Any],
						  status_symbols: Dict[str, StatusSymbols]) -> Optional[Assertion]:
		return

	@staticmethod
	def create_death_conditions(info: GameInformation, possible_statuses: Dict[str, Any],
								status_symbols: Dict[str, StatusSymbols]) -> Optional[List[List[DatatypeRef]]]:
		return
