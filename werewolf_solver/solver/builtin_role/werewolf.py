from typing import *

from z3 import *

from werewolf_solver.model.assertion import Assertion
from werewolf_solver.model.game_information import GameInformation, get_alive_players
from werewolf_solver.solver.role import Role
from werewolf_solver.solver.status_symbols import StatusSymbols


class Werewolf(Role):
	"""

	"""

	@property
	def name(self):
		return "Werewolf"

	@staticmethod
	def create_symbol(info: GameInformation) -> Optional[Tuple[str, StatusSymbols]]:
		return

	@staticmethod
	def create_status(info: GameInformation, status_symbols: Dict[str, StatusSymbols]) -> Optional[
		Tuple[str, Any]]:
		# X日目に襲撃されたプレイヤーは誰か？という状態を持つ
		attacked_statuses = [
			Const(f"attacked_{i}", status_symbols["players_index"])
			for i in range(len(info["corpses"]))
		]
		return "attacked_players", attacked_statuses

	@staticmethod
	def serialize_status(model: ModelRef, possible_statuses: Dict[str, Any]) -> Optional[Tuple[str, Any]]:
		return "attacked_players", [model[attacked_player] for attacked_player in possible_statuses["attacked_players"]]

	def create_assertions(self, info: GameInformation, possible_statuses: Dict[str, Any],
						  status_symbols: Dict[str, StatusSymbols]) -> Optional[Tuple[Assertion, DatatypeRef]]:
		players_role = possible_statuses["players_role"]
		role_symbols = status_symbols["players_role"]
		assertions: List[Assertion] = []
		death_conditions = self.create_death_conditions(info, possible_statuses, status_symbols)
		for day, attacked_player in enumerate(possible_statuses["attacked_players"]):
			attacked_player: Int
			# 人狼は噛めない(身内噛み無し)
			assertions.append(players_role[attacked_player] != role_symbols.get("Werewolf"))
			# 必ず生きてる人を噛む
			assertions.append(Or([attacked_player == player for player in get_alive_players(info, day)]))
			is_succeed_attack = death_conditions[day][0]
			# 襲撃に成功したら死体に噛んだ対象がいる
			assertions.append(Implies(
				is_succeed_attack,
				Or([
					attacked_player == corpse
					for corpse in info["corpses"][day]
				])
			))

		return And(assertions)

	@staticmethod
	def create_death_conditions(info: GameInformation, possible_statuses: Dict[str, Any],
								status_symbols: Dict[str, StatusSymbols]) -> Optional[List[List[DatatypeRef]]]:
		death_conditions = []
		for day, attacked_player in enumerate(possible_statuses["attacked_players"]):
			attacked_player: Int
			is_failed_attack = False
			if "Knight" in info["casting"] and day != 0:
				# 狩人の結果は2日目から
				is_failed_attack = Or(
					is_failed_attack,
					attacked_player == possible_statuses["saved_players"][day - 1]
				)
			if "Fox" in info["casting"]:
				# 妖狐は噛めない
				is_failed_attack = Or(
					is_failed_attack,
					possible_statuses["players_role"][attacked_player] == status_symbols["players_role"].get("Fox")
				)
			death_conditions.append([Not(is_failed_attack)])
		return death_conditions
