from typing import *

from z3 import *

from werewolf_solver.model.assertion import Assertion
from werewolf_solver.model.game_information import GameInformation
from werewolf_solver.solver.role import Role
from werewolf_solver.solver.status_symbols import StatusSymbols


class MediumResult(TypedDict):
	target: int
	is_werewolf: bool


class Medium(Role):
	@property
	def name(self):
		return "Medium"

	@staticmethod
	def create_symbol(info: GameInformation) -> Optional[Tuple[str, StatusSymbols]]:
		return

	@staticmethod
	def create_status(info: GameInformation, status_symbols: Dict[str, StatusSymbols]) -> Optional[
		Tuple[str, Any]]:
		return

	@staticmethod
	def serialize_status(model: ModelRef, possible_statuses: Dict[str, Any]) -> Optional[Tuple[str, Any]]:
		return

	def create_assertions(self, info: GameInformation, possible_statuses: Dict[str, Any],
						  status_symbols: Dict[str, StatusSymbols]) -> Optional[Assertion]:
		players_role = possible_statuses["players_role"]
		role_symbol = status_symbols["players_role"]
		assertions: List[Assertion] = []
		coming_outs = info["co_table"][self.name]
		for coming_out in coming_outs:
			player_assertions = []
			for result in coming_out["results"]:
				if result["is_werewolf"]:
					player_assertions.append(
						players_role[result["target"]] == role_symbol.get("Werewolf"))
				else:
					player_assertions.append(
						players_role[result["target"]] != role_symbol.get("Werewolf"))
			assertions.append(Implies(
				players_role[coming_out["index"]] == role_symbol.get("Medium"),
				And(player_assertions)
			))
		return And(assertions)

	@staticmethod
	def create_death_conditions(info: GameInformation, possible_statuses: Dict[str, Any],
								status_symbols: Dict[str, StatusSymbols]) -> Optional[List[List[DatatypeRef]]]:
		return
