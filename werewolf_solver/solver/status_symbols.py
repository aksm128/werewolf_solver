from typing import *

from z3 import *


class StatusSymbols:
	"""
	状態を表すSymbol
	"""

	def __init__(self, name: str, role_strings: List[str]) -> None:
		"""
		:param role_strings:
		"""
		self.status_strings = role_strings
		self.enum, self.symbols = EnumSort(name, role_strings)

	def get(self, s: str) -> Optional[DatatypeRef]:
		"""
		文字列からSymbolを取得する
		:param str s:
		:return Optional[DatatypeRef]:
		"""
		enum_index = self.status_strings.index(s)
		if enum_index == -1:
			return
		return self.symbols[enum_index]
