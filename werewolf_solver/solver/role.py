from abc import abstractmethod, ABC
from typing import *

from z3 import *

from werewolf_solver.model.assertion import Assertion
from werewolf_solver.model.game_information import GameInformation
from werewolf_solver.solver.status_symbols import StatusSymbols


class Role(ABC):
	@property
	@abstractmethod
	def name(self) -> str:
		pass

	@staticmethod
	@abstractmethod
	def create_symbol(info: GameInformation) -> Optional[Tuple[str, StatusSymbols]]:
		"""
		その事柄に関わる状態を識別するsymbolを返す(0,1かを宣言して使う役職があったとした時の0,1のこと)
		:param info:
		:return:
		"""
		pass

	@staticmethod
	@abstractmethod
	def create_status(info: GameInformation, status_symbols: Dict[str, StatusSymbols]) -> Optional[
		Tuple[str, Any]]:
		"""
		その事柄に関わる状態の可能性を表すstatusを返す。(狼であれば噛み先はどこかなど)
		:param info:
		:param status_symbols:
		:return:
		"""
		pass

	@staticmethod
	@abstractmethod
	def serialize_status(model: ModelRef, possible_statuses: Dict[str, Any]) -> Optional[Tuple[str, Any]]:
		"""
		modelからPython上のオブジェクトに変換する。ついでに結果の名前も。
		:param model:
		:param possible_statuses:
		:return:
		"""
		pass

	@staticmethod
	@abstractmethod
	def create_assertions(info: GameInformation, possible_statuses: Dict[str, Any],
						  status_symbols: Dict[str, StatusSymbols]) -> Optional[Assertion]:
		"""
		その事柄に関わる状態から新たな制約を返す
		:param info:
		:param possible_statuses:
		:param status_symbols:
		:return:
		"""
		pass

	@staticmethod
	@abstractmethod
	def create_death_conditions(info: GameInformation, possible_statuses: Dict[str, Any],
								status_symbols: Dict[str, StatusSymbols]) -> Optional[List[List[DatatypeRef]]]:
		"""
		死体ができる条件をリストにして返す
		:param info:
		:param possible_statuses:
		:param status_symbols:
		:return:
		"""
		pass
