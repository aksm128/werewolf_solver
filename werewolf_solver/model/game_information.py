from typing import *

from werewolf_solver.model.assertion import Assumptions

"""
配役を決める辞書。
{役職名: 役職数}
"""
Casting = Dict[str, int]

"""
処刑された人。2日目から。
"""
Executed = List[Optional[List[int]]]

"""
無残な死体で発見された人たち。初日から。
"""
Corpses = List[Optional[List[int]]]


class ComingOut(TypedDict):
	index: int
	results: Any


"""
key: 役職名
value: 各人のカミングアウト。
"""
COTable = Dict[str, List[ComingOut]]


class GameInformation(TypedDict):
	casting: Casting
	co_table: COTable
	executed: Executed
	corpses: Corpses
	assumptions: Assumptions


def count_player(casting: Casting) -> int:
	return sum([casting[key] for key in casting])


def count_day(info: GameInformation) -> int:
	return len(info["corpses"]) + 1


def is_valid_game_info(info: GameInformation) -> bool:
	return len(info["corpses"]) == len(info["executed"]) + 1


def get_dead_players(info: GameInformation, day: int) -> List[int]:
	dead_players = []
	for corpse, executed in zip(info["corpses"][:day], info["executed"][:day]):
		dead_players += corpse
		dead_players += executed
	return dead_players


def get_alive_players(info: GameInformation, day: int):
	dead_players = get_dead_players(info, day)
	return list(filter(lambda pl: pl not in dead_players, range(count_player(info["casting"]))))
