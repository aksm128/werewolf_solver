from typing import *


class Assertion(TypedDict):
	"""
	そのプレイヤーが真だった場合に能力で確定できること。占い師であれば占い結果。狩人であればGJ先。
	あくまで能力による確定であり、盤面までは考慮しなくてよい。
	型は(言及するプレイヤー番号, ["可能性のある役職1", "役職2",...], その役職を肯定するならtrue)
	"""
	target: int
	roles: List[str]
	truth: bool


class PlayerAssertions(TypedDict):
	"""
	player_num: プレイヤーに割り振られた番号
	assertions: 確定できる事柄。Assertionを参照。
	例:
		1: 占いCO 2○ → 3●であれば
		{
			"player_num": 1,
			"assertions": [
				{"target": 2, "roles": ["人狼"], "truth": false}
				{"target": 3, "roles": ["人狼"], "truth": true}
			]
		}
	"""
	player_num: int
	assertions: List[Assertion]


# 意味的にはList[RoleComingOut]が正しいけどこっちの方が楽
"""
役職ごとのプレイヤーのCO。
{役職名: List[ComingOut]}
"""
RoleAssertionTable = Dict[str, List[PlayerAssertions]]

"""
外部から入力される仮定。村騙りや自視点確定情報などの制限を扱う。
"""
Assumptions = List[Assertion]
