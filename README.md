# Werewolf solver
人狼ゲームの内訳をSAT/SMTソルバ(z3py)で列挙するプログラム  
- 8CFOなどの情報が多い盤面は高速に処理できる
- 普通配役などのグレーがある程度存在する盤面は内訳のケースが広いので処理が終わらない

# Usage
簡単な例
```python
from werewolf_solver.solver.solver import search_cases
# 配役 (占狼村村)
casting = {
	"Werewolf": 1,
	"Villager": 2,
	"FortuneTeller": 1,
}
# CO表 (プレイヤー1が占いCOをし、プレイヤー2黒と主張した)
co_table = {
	"FortuneTeller": [{
		"index": 1,
		"results": [{"target": 2, "is_werewolf": True}]
	}]
}
# 処刑したプレイヤー
executed = []
# 朝に死体になったプレイヤー (プレイヤー0は初日に襲撃された)
corpses = [[0]]
# 仮定 (プレイヤー1は占い師)
assumptions = [{
	"target": 1, "roles": ["FortuneTeller"], "truth": True
}]
# まとめた情報
info = {
	"casting": casting,
	"co_table": co_table,
	"executed": executed,
	"corpses": corpses,
	"assumptions": assumptions
}

# このような条件を満たす内訳を全探索
print(search_cases(info))
"""
結果 => [{
    # 占ったプレイヤー
    'fortune_told_players': [2], 
    # 襲撃したプレイヤー
    'attacked_players': [0], 
    # 配役
    'players_role': ['Villager', 'FortuneTeller', 'Werewolf', 'Villager']
}]
"""
```
8CFOのような配役も問題なく整理できる。
```python
from werewolf_solver.model.assertion import Assumptions
from werewolf_solver.model.game_information import Casting, COTable, Executed, Corpses, GameInformation
from werewolf_solver.solver.solver import search_cases

# 8Cの村人を狩人にした配役
casting: Casting = {
	"Knight": 1,
	"FortuneTeller": 1,
	"Medium": 1,
	"Sharer": 2,
	"Madman": 1,
	"Werewolf": 2
}

"""
占い師
1: 4○ 5○
2: 7●
3: 7●
霊能
4: ○
共有者
5-6
狩人
7: 1護衛
処刑
2
死体
初日 → 平和
"""

co_table: COTable = {
	"FortuneTeller": [{
		"index": 1,
		"results": [{"target": 4, "is_werewolf": False}, {"target": 5, "is_werewolf": False}]
	}, {
		"index": 2,
		"results": [{"target": 7, "is_werewolf": True}]
	}, {
		"index": 3,
		"results": [{"target": 7, "is_werewolf": True}, {"target": 5, "is_werewolf": False}]
	}],
	"Medium": [{
		"index": 4,
		"results": [{"target": 2, "is_werewolf": False}]
	}],
	"Sharer": [{
		"index": 5,
		"results": [{"partner": 6}]
	}, {
		"index": 6,
		"results": [{"partner": 5}]
	}],
	"Knight": [{
		"index": 7,
		"results": [{"target": 1}]
	}]
}

executed: Executed = [[2]]

# 死体無しなら空配列とする
corpses: Corpses = [[0], []]

# 村騙りを制限
assumptions: Assumptions = [{
	"target": 1, "roles": ["FortuneTeller", "Madman", "Werewolf"], "truth": True
}, {
	"target": 2, "roles": ["FortuneTeller", "Madman", "Werewolf"], "truth": True
}, {
	"target": 3, "roles": ["FortuneTeller", "Madman", "Werewolf"], "truth": True
}, {
	"target": 4, "roles": ["Medium", "Madman", "Werewolf"], "truth": True
}, {
	"target": 5, "roles": ["Sharer", "Madman", "Werewolf"], "truth": True
}, {
	"target": 6, "roles": ["Sharer", "Madman", "Werewolf"], "truth": True
}, {
	"target": 7, "roles": ["Knight", "Madman", "Werewolf"], "truth": True
}]

info: GameInformation = {
	"casting": casting,
	"co_table": co_table,
	"executed": executed,
	"corpses": corpses,
	"assumptions": assumptions
}

print(search_cases(info))
"""
[{
	'fortune_told_players': [4, 5], 
	'attacked_players': [0, 1], 
	'saved_players': [1], 
	'players_role': ['Medium', 'FortuneTeller', 'Werewolf', 'Werewolf', 'Madman', 'Sharer', 'Sharer', 'Knight']
}]
"""
```
## Note
現在実装しているのは村人・占い師・霊能・狩人・共有者・狂人・人狼・妖狐。  
CO結果の型はwerewolf_solver/solver/builtin_role/の中にある定義を参照すること。  
assumptionsは「targetの役職はrolesの中にある役職である/ない(truth)」という形式で仮定を定義する。

# 未実装の要素
- 占い師・狩人の複数人対応
- 「自分は占えない」「自分は護衛できない」のロジックの追加
- 勝利条件のロジックの追加
- 村騙り禁止の制約を簡単につけられる関数の追加
# 問題点
- 計算量  
    N人配役の内訳はだいたいN!通り、それに護衛先などの役職の不確定情報までつけると計算量が爆発する  
    → 実用を目指すなら全探索は厳しい
    - ケースの列挙でなく可能性の列挙であれば間に合う？
    - 役職COしている人に限って列挙する？
- 役職
    - 特殊なタイミングでイベントを起こせる役職に対して対応できるか怪しい
    - 鯖ごとの挙動の違いをどうするか
